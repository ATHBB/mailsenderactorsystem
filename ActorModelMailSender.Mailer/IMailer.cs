﻿using ActorModelMailSender.Data.Messages;
using System.Threading.Tasks;

namespace ActorModelMailSender.Mailer
{
    public interface IMailer
    {
        Task SendMail(MailDataMessage data);
    }
}