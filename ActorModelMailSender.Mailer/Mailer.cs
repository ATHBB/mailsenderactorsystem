﻿using ActorModelMailSender.Data;
using ActorModelMailSender.Data.Messages;
using FluentMailer.Factory;
using FluentMailer.Interfaces;
using System;
using System.Threading.Tasks;

namespace ActorModelMailSender.Mailer
{
    public class Mailer : IMailer
    {
        readonly IFluentMailer _FluentMailer;

        public Mailer()
        {
            _FluentMailer = FluentMailerFactory.Create();
        }

        public async Task SendMail(MailDataMessage data)
        {
            try
            {
                await _FluentMailer.CreateMessage()
                       .WithView(ApplicationSettings.MailTemplate, data)
                      .WithReceiver(data.Email)
                      .WithSubject("Zostań konsultantem AVON!!")
                      .SendAsync();
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
