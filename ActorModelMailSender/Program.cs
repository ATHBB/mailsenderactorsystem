﻿using ActorModelMailSender.Data;
using ActorModelMailSender.Logger;
using ActorModelMailSender.Service;
using Ninject;
using System.Configuration;
using System.Reflection;

namespace ActorModelMailSender
{
    class Program
    {
        static void Main()
        {
            GetConfigurationValues();
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            kernel.Get<ILogger>().InitializeLogger();
            kernel.Get<IService>().Initialize();
        }

        static void GetConfigurationValues()
        {
            int iteration = 0;
            string iter = ConfigurationManager.AppSettings["iteration"];
            int.TryParse(iter, out iteration);
            ApplicationSettings.DataFilePath = ConfigurationManager.AppSettings["dataFilePath"];
            ApplicationSettings.Iteration = iteration;
            ApplicationSettings.MailTemplate = ConfigurationManager.AppSettings["mailTemplate"];
            ApplicationSettings.ServiceName = ConfigurationManager.AppSettings["serviceName"];
        }
    }
}
