﻿using ActorModelMailSender.Logger;
using ActorModelMailSender.Mailer;
using ActorModelMailSender.Parser;
using ActorModelMailSender.Runner;
using ActorModelMailSender.Service;
using Ninject.Modules;

namespace ActorModelMailSender.DIProvider
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<ILogger>().To<Logger.Logger>().InSingletonScope();
            Bind<IMailer>().To<Mailer.Mailer>().InSingletonScope();
            Bind<IFileParser>().To<FileParser>().InSingletonScope();
            Bind<IApplicationRunner>().To<ApplicationRunner>().InSingletonScope();
            Bind<IService>().To<Service.Service>().InSingletonScope();
        }
    }
}
