﻿using ActorModelMailSender.Data;
using ActorModelMailSender.Logger;
using ActorModelMailSender.Mailer;
using ActorModelMailSender.Parser;
using ActorModelMailSender.Runner;
using System;
using Topshelf;

namespace ActorModelMailSender.Service
{
    public class Service : IService
    {
        readonly ILogger _logger;
        readonly IFileParser _fileParser;
        readonly IMailer _mailer;

        public Service(ILogger logger, IFileParser fileParser, IMailer mailer)
        {
            _logger = logger;
            _fileParser = fileParser;
            _mailer = mailer;
        }

        public void Initialize()
        {
            try
            {
                HostFactory.Run(configure =>
                {
                    configure.Service<IApplicationRunner>(service =>
                    {
                        service.ConstructUsing(() => new ApplicationRunner(_fileParser, _logger, _mailer));
                        service.WhenStarted(s => s.Start());
                        service.WhenStopped(s => s.Stop());
                    });
                    configure.RunAsLocalSystem();
                    configure.SetServiceName(ApplicationSettings.ServiceName);
                    configure.SetDisplayName(ApplicationSettings.ServiceName);
                    configure.SetDescription("Usługa na zajecia");
                });
            }
            catch (Exception ex)
            {
                _logger.AddLogError("An error occurred while creating service", ex);
            }
        }
    }
}
