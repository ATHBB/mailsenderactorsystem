﻿using System;

namespace ActorModelMailSender.Logger
{
    public interface ILogger
    {
        void AddLogEntry(string text);
        void AddLogError(string text, Exception ex);
        void InitializeLogger();
    }
}