﻿namespace ActorModelMailSender.Data
{
    public class MailData
    {
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
    }
}
