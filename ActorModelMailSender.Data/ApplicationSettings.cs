﻿namespace ActorModelMailSender.Data
{
    public static class ApplicationSettings
    {
        public static string DataFilePath { get; set; }
        public static int Iteration { get; set; }
        public static string MailTemplate { get; set; }
        public static string ServiceName { get; set; }
    }
}
