﻿namespace ActorModelMailSender.Data.Messages
{
    public class MailDataMessage
    {
        public string Email { get; }
        public string Name { get; }
        public string SecondName { get; }

        public MailDataMessage(string email, string name, string secondName)
        {
            Email = email;
            Name = name;
            SecondName = secondName;
        }
    }
}
