﻿namespace ActorModelMailSender.Data.Messages
{
    public class FileParserMessage
    {
        public string DataFilePath { get; }
        public int Iteration { get; }

        public FileParserMessage(string dataFilePath, int iteration)
        {
            DataFilePath = dataFilePath;
            Iteration = iteration;
        }
    }
}
