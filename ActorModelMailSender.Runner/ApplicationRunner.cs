﻿using ActorModelMailSender.Logger;
using ActorModelMailSender.Mailer;
using ActorModelMailSender.Parser;
using ActorModelMailSender.Runner.Actors;
using Akka.Actor;
using Akka.Routing;

namespace ActorModelMailSender.Runner
{
    public class ApplicationRunner : IApplicationRunner
    {
        ActorSystem _system;
        readonly IFileParser _fileParser;
        readonly ILogger _logger;
        readonly IMailer _mailer;
        IActorRef mailSenderRouter;
        IActorRef fileParserActor;

        public ApplicationRunner(IFileParser _fileParser, ILogger _logger, IMailer _mailer)
        {
            this._fileParser = _fileParser;
            this._logger = _logger;
            this._mailer = _mailer;
        }

        public void Start()
        {
            _system = ActorSystem.Create("Sender");
            var props = Props.Create(() => new MailSenderActor(_mailer, _logger)).WithRouter(FromConfig.Instance);
            mailSenderRouter = _system.ActorOf(props, "MailSenderActor");
            fileParserActor = _system.ActorOf(Props.Create(() => new FileParserActor(_fileParser, _logger, _mailer, mailSenderRouter)));
        }

        public void Stop()
        {
            if (mailSenderRouter != null)
                mailSenderRouter.Tell(new Broadcast(PoisonPill.Instance));
            if (fileParserActor != null)
                fileParserActor.Tell(new Broadcast(PoisonPill.Instance));
        }
    }
}
