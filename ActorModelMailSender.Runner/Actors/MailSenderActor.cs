﻿using ActorModelMailSender.Data.Messages;
using ActorModelMailSender.Logger;
using ActorModelMailSender.Mailer;
using Akka.Actor;
using System;
using System.Threading.Tasks;

namespace ActorModelMailSender.Runner.Actors
{
    public class MailSenderActor : ReceiveActor
    {
        readonly IMailer _mailer;
        readonly ILogger _logger;

        public MailSenderActor(IMailer mailer, ILogger logger)
        {
            _mailer = mailer;
            _logger = logger;
            ReceiveAsync<MailDataMessage>(x => Handle(x));
        }

        public async Task Handle(MailDataMessage message)
        {
            _logger.AddLogEntry($"Sending mail to: {message.Email} ...");
            await SendMail(message);
        }

        async Task SendMail(MailDataMessage message)
        {
            try
            {
                await _mailer.SendMail(message);
            }
            catch (Exception ex)
            {
                _logger.AddLogError($"An error occurred in method:{nameof(SendMail)}", ex);
            }
        }

    }
}
