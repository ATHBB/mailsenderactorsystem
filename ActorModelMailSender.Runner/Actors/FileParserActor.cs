﻿using ActorModelMailSender.Data;
using ActorModelMailSender.Data.Messages;
using ActorModelMailSender.Logger;
using ActorModelMailSender.Mailer;
using ActorModelMailSender.Parser;
using Akka.Actor;
using System;

namespace ActorModelMailSender.Runner.Actors
{
    public class FileParserActor : ReceiveActor
    {
        ICancelable _cancelable;
        readonly IFileParser _fileParser;
        readonly ILogger _logger;
        readonly IMailer _mailer;
        readonly IActorRef _router;

        public FileParserActor(IFileParser fileParser, ILogger logger, IMailer mailer, IActorRef router)
        {
            _logger = logger;
            _fileParser = fileParser;
            _mailer = mailer;
            _router = router;
            Receive<FileParserMessage>(message => Handle(message));

        }

        protected override void PreStart()
        {
            _cancelable = Context.System.Scheduler.ScheduleTellRepeatedlyCancelable(
                TimeSpan.Zero,
                TimeSpan.FromSeconds(60),
                Self,
                new FileParserMessage(ApplicationSettings.DataFilePath, ApplicationSettings.Iteration),
                Self);
        }

        protected override void PostStop()
        {
            _cancelable.Cancel();
            _logger.AddLogEntry("Scheduler canceled");
        }

        void Handle(FileParserMessage message)
        {
            try
            {
                _logger.AddLogEntry($"Getting data for iteration:{ApplicationSettings.Iteration}");
                var readedData = _fileParser.GetDatFromFileFile(message.DataFilePath, ApplicationSettings.Iteration);
                ApplicationSettings.Iteration += 1;
                readedData.ForEach(x =>
                {
                    _router.Tell(new MailDataMessage(x.Email, x.Name, x.SecondName));
                });
            }
            catch (Exception ex)
            {
                _logger.AddLogError(nameof(Handle), ex);
            }
        }
    }
}
