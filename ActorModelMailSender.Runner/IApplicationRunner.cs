﻿namespace ActorModelMailSender.Runner
{
    public interface IApplicationRunner
    {
        void Start();
        void Stop();
    }
}