﻿using ActorModelMailSender.Data;
using CsvHelper;
using CsvHelper.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ActorModelMailSender.Parser
{
    public class FileParser : IFileParser
    {
        public List<MailData> GetDatFromFileFile(string filePath, int iteration)
        {
            if (File.Exists(filePath))
            {
                using (var sw = new StreamReader(filePath))
                {
                    var csv = new CsvReader(sw, new CsvConfiguration
                    {
                        HasHeaderRecord = false,
                        Delimiter = ";"
                    });
                    return csv.GetRecords<MailData>().Skip(iteration * 100).Take(100).ToList();
                }
            }
            return new List<MailData>();
        }
    }
}
