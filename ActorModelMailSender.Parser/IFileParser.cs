﻿using System.Collections.Generic;
using ActorModelMailSender.Data;

namespace ActorModelMailSender.Parser
{
    public interface IFileParser
    {
        List<MailData> GetDatFromFileFile(string filePath, int iteration);
    }
}